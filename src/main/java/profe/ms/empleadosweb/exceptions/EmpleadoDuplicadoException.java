package profe.ms.empleadosweb.exceptions;

public class EmpleadoDuplicadoException extends EmpleadosException {

	public EmpleadoDuplicadoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmpleadoDuplicadoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public EmpleadoDuplicadoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EmpleadoDuplicadoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EmpleadoDuplicadoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
